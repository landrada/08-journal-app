import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyDXpBtXrQQ_PnxmFSw0O-3jwx5xc5vumf0",
    authDomain: "react-app-cursos-2020.firebaseapp.com",
    databaseURL: "https://react-app-cursos-2020.firebaseio.com",
    projectId: "react-app-cursos-2020",
    storageBucket: "react-app-cursos-2020.appspot.com",
    messagingSenderId: "785765194213",
    appId: "1:785765194213:web:10ae2e73738ab7e478370c"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();


export {
    db,
    googleAuthProvider,
    firebase
}